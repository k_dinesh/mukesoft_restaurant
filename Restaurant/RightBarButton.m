//
//  RightBarButton.m
//  Restaurant
//
//  Created by HN on 03/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "RightBarButton.h"

@implementation RightBarButton

- (id)initWithCoder:(NSCoder*)coder
{
    self = [super initWithCoder:coder];
    
    if (self) {
        
        [self setTitleTextAttributes:@{
                                       NSFontAttributeName: [UIFont systemFontOfSize:13.0 weight:UIFontWeightMedium],
                                       NSForegroundColorAttributeName: [UIColor whiteColor]
                                       } forState:UIControlStateNormal];
        
    }
    
    return self;
    
}

@end

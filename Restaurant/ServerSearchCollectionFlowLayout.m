//
//  ServerSearchCollectionFlowLayout.m
//  Restaurant
//
//  Created by HN on 19/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "ServerSearchCollectionFlowLayout.h"




@implementation ServerSearchCollectionFlowLayout


/*
- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.minimumLineSpacing = 5.0;
        self.minimumInteritemSpacing = 1.0;
//        self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        self.scrollDirection = UICollectionViewScrollDirectionVertical;

        
    }
    return self;
}

- (CGSize)itemSize
{

    CGFloat cellWidth = self.collectionView.frame.size.width / 3;
    CGFloat cellHeight = self.collectionView.frame.size.width / 2;
    
    return CGSizeMake(cellWidth, cellHeight);
    
}
*/


- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.minimumLineSpacing = 2.0;
        self.minimumInteritemSpacing = 1.0;
        
        //        self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        self.scrollDirection = UICollectionViewScrollDirectionVertical;
        
    }
    return self;
}
- (CGSize)itemSize
{
    NSInteger numberOfColumns = 3;
    
    CGFloat itemWidth = (CGRectGetWidth(self.collectionView.frame) - (numberOfColumns - 1)) / numberOfColumns;
    CGFloat itemHeight = (CGRectGetHeight(self.collectionView.frame)) / 1.8;
    return CGSizeMake(itemWidth, itemHeight);
    
}



@end

//
//  Servers.h
//  Restaurant
//
//  Created by HN on 25/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <Realm/Realm.h>


@interface Servers : RLMObject

@property NSInteger serverID;
@property NSString *email;
@property NSString *city;
@property NSString *imageURL;
@property NSString *serverName;
@property NSString *restaurantName;
@property float avgRating;
@property NSInteger workingRestroID;
@property NSInteger toalRatings;

//@property (readonly) RLMLinkingObjects *rstaurants;
@end

// This protocol enables typed collections. i.e.:
// RLMArray<Servers>
RLM_ARRAY_TYPE(Servers)

//
//  ForgotPasswordViewController.h
//  Restaurant
//
//  Created by HN on 09/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking.h>

@interface ForgotPasswordViewController : UIViewController<UITextFieldDelegate>


@property (weak, nonatomic) IBOutlet UITextField *txtEmailAddress;

- (IBAction)btnSubmit:(id)sender;



@end

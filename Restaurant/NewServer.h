//
//  NewServer.h
//  Restaurant
//
//  Created by HN on 01/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <Realm/Realm.h>

@interface NewServer : RLMObject

@property NSString *profilePic;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<NewServer>
RLM_ARRAY_TYPE(NewServer)

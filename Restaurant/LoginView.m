//
//  LoginView.m
//  Restaurant
//
//  Created by HN on 13/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "LoginView.h"
#import "SignUpViewController.h"

@interface LoginView ()
{
    BOOL toggle;
    UIActivityIndicatorView *activityView;
}

@end

@implementation LoginView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault]; //UIImageNamed:@"transparent.png"
    self.navigationController.navigationBar.shadowImage = [UIImage new];////UIImageNamed:@"transparent.png"
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    [_usernameText setDelegate:self];
    [_passwordText setDelegate:self];
    activityView = [[UIActivityIndicatorView alloc]
                    initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    activityView.center=self.view.center;
    [activityView hidesWhenStopped];
    [self.view addSubview:activityView];
    
    //    if([[[NSUserDefaults standardUserDefaults] valueForKey:@"isLoggedIn"] isEqualToString:@"Yes"])
    //    {
    //        toggle = YES;
    //        NSLog(@"%d",toggle);
    //    }
    //    else{
    //        toggle = NO;
    //        NSLog(@"%d",toggle);
    //    }
    
    toggle= YES;
    
    _btnGooglePlus.layer.cornerRadius = _btnGooglePlus.frame.size.width/2;
    _btnTwitter.layer.cornerRadius = _btnGooglePlus.frame.size.width/2;
    _btnFacebook.layer.cornerRadius = _btnGooglePlus.frame.size.width/2;
    
    
    [GIDSignIn sharedInstance].delegate = self;
    
    
    _btnFaceBook.readPermissions =
    @[@"public_profile", @"email", @"user_friends"];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:YES];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
}

- (IBAction)rememberMePressed:(id)sender {
    
    if (toggle) {
        [[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:@"isLoggedIn"];
        NSLog(@"%d",toggle);
    }
    else{
        [[NSUserDefaults standardUserDefaults] setValue:@"no" forKey:@"isLoggedIn"];
        NSLog(@"%d",toggle);
    }
    toggle = !toggle;
    
    [_btnRemeberMe setImage:[UIImage imageNamed:toggle ? @"un-selected.png" :@"selected.png"] forState:UIControlStateNormal];
    
}
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    
    if(user == nil)
    {
        // Perform any operations on signed in user here.
        //        NSString *userId = user.userID;                  // For client-side use only!
        //        NSString *idToken = user.authentication.idToken; // Safe to send to the server
        //        NSString *fullName = user.profile.name;
        //        NSString *givenName = user.profile.givenName;
        //        NSString *familyName = user.profile.familyName;
        //        NSString *email = user.profile.email;
        NSLog(@"");
        [[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:@"isuserloggedin"];
        
        
        
        
        // ...
    }
    else{
        NSLog(@"User Already logged in..");
        if([[[NSUserDefaults standardUserDefaults] valueForKey:@"Sender"] isEqualToString:@"Home"]){
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertController * alert2=   [UIAlertController
                                               alertControllerWithTitle:@"Login Sucessful"
                                               message:@""
                                               preferredStyle:UIAlertControllerStyleAlert];
                
                [self presentViewController:alert2 animated:YES completion:nil];
                
                
                
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [alert2 dismissViewControllerAnimated:YES completion:nil];
                    [self.navigationController popViewControllerAnimated:YES];
                    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
                });
                
            });
            
            
        }
        //        [[NSUserDefaults standardUserDefaults] setValue:@"no" forKey:@"isuserloggedin"];
        
    }
}

- (IBAction)loginButtonPressed:(id)sender {
    
    
    if (([_usernameText.text isEqualToString:@""] || ([_passwordText.text isEqualToString:@""])))
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@""
                                           message:@"Enter Username and Password"
                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:YES completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [alert2 dismissViewControllerAnimated:YES completion:nil];
            });
            [self.navigationController popViewControllerAnimated:YES];
        });
    }
    else
    {
        [activityView startAnimating];
        NSError *error;
        
        
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,SIGN_IN_URL]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setHTTPMethod:@"POST"];
        
        NSMutableDictionary *dataDict = [NSMutableDictionary new];
        
        [dataDict setValue:_usernameText.text forKey:@"username"];
        [dataDict setValue:_passwordText.text forKey:@"password"];
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:dataDict
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        
        NSString *postStr;
        if (! postData) {
            NSLog(@"Got an error: %@", error);
            
        } else {
            NSString *jsonString = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
            
            postStr = [NSString stringWithFormat:@"[%@]",jsonString];
            
        }
        
        [request setHTTPBody:postData];
        
        
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            
            NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            NSLog(@"Response: %@",dataString);
            
            double status = (long)[httpResponse statusCode];
            NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
            if (data.length > 0) {
                
                NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                if (error != nil) {
                    NSLog(@"Error parsing JSON.");
                }
                else {
                    
                    NSLog(@"Dict: %@", jsonDict);
                    
                    if (status == 202){
                        
                        RLMRealm *realm = [RLMRealm defaultRealm];
                        
                        [activityView stopAnimating];
                        NSLog(@"You may log in");
                        
                        
                        
                        
                        [realm beginWriteTransaction];
                        
                        NSDictionary *data = [jsonDict valueForKey:@"data"];
                        
                        
                        NSString *userName = [data valueForKey:@"username"];
                        NSLog(@"User:%@",userName);
                        [[NSUserDefaults standardUserDefaults] setValue:userName forKey:@"username"];
                        
                        NSString *token = [data valueForKey:@"token"];
                        NSLog(@"Token:%@",token);
                        [[NSUserDefaults standardUserDefaults] setValue:token forKey:@"token"];
                        
                        AppUser *newUser = [[AppUser alloc] init];
                        newUser.token = token;
                        [realm addObject:newUser];
                        
                        [realm commitWriteTransaction];
                        
                        
                        
                        
                        
                        [[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:@"isuserloggedin"];
                        
                        if([[[NSUserDefaults standardUserDefaults] valueForKey:@"Sender"] isEqualToString:@"Home"])
                        {
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                UIAlertController * alert2=   [UIAlertController
                                                               alertControllerWithTitle:@"Login Sucessful"
                                                               message:@""
                                                               preferredStyle:UIAlertControllerStyleAlert];
                                
                                [self presentViewController:alert2 animated:YES completion:nil];
                                
                                
                                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                    [alert2 dismissViewControllerAnimated:YES completion:nil];
                                    [self.navigationController popViewControllerAnimated:YES];
                                    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
                                });
                                
                            });
                            
                            
                        }
                        
                        if([[[NSUserDefaults standardUserDefaults] valueForKey:@"Sender"] isEqualToString:@"Comment"]) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                UIAlertController * alert2=   [UIAlertController
                                                               alertControllerWithTitle:@"Login Sucessful"
                                                               message:@""
                                                               preferredStyle:UIAlertControllerStyleAlert];
                                
                                [self presentViewController:alert2 animated:YES completion:nil];
                                
                                
                                
                                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                    [alert2 dismissViewControllerAnimated:YES completion:nil];
                                    [self.navigationController popViewControllerAnimated:YES];
                                    [self.navigationController dismissViewControllerAnimated:NO completion:nil];
                                });
                                
                            });
                        }
                        
                    }
                    if (status == 400){
                        [activityView stopAnimating];
                        NSLog(@"Login Failed");
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            UIAlertController * alert2=   [UIAlertController
                                                           alertControllerWithTitle:@"Error"
                                                           message:[jsonDict valueForKey:@"result"]
                                                           preferredStyle:UIAlertControllerStyleAlert];
                            
                            [self presentViewController:alert2 animated:YES completion:nil];
                            
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                [alert2 dismissViewControllerAnimated:YES completion:nil];
                            });
                        });
                        
                    }
                    if (status == 401){
                        [activityView stopAnimating];
                        NSLog(@"Login Failed");
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            UIAlertController * alert2=   [UIAlertController
                                                           alertControllerWithTitle:@"Error"
                                                           message:@"User Not Authenticated"
                                                           preferredStyle:UIAlertControllerStyleAlert];
                            
                            [self presentViewController:alert2 animated:YES completion:nil];
                            
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                [alert2 dismissViewControllerAnimated:YES completion:nil];
                            });
                        });
                        
                    }
                    
                    
                }
            }
            else {
                [activityView stopAnimating];
                NSLog(@"Login Failed");
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIAlertController * alert2=   [UIAlertController
                                                   alertControllerWithTitle:@"Error"
                                                   message:error.localizedDescription
                                                   preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert2 animated:YES completion:nil];
                    
                    
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [alert2 dismissViewControllerAnimated:YES completion:nil];
                    });
                });
            }
            
            
        }];
        
        [postDataTask resume];
        
    }
    
    _passwordText.text=@"";
    _usernameText.text=@"";
    [self.view endEditing:YES];
    [activityView stopAnimating];
    
}
- (IBAction)forgotPasswordButton:(id)sender {
    
    NSLog(@"Forgot Password Pressed...");
    
//    ForgotPasswordViewController *FPVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPasswordViewController"];
//    
//    [self.navigationController pushViewController:FPVC animated:YES];
//    
}
- (IBAction)registerButtonPressed:(id)sender {
    
    SignUpViewController *signUp = [self.storyboard instantiateViewControllerWithIdentifier:@"SignUpViewController"];
    [self.navigationController pushViewController:signUp animated:YES];
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn
presentViewController:(UIViewController *)viewController {
    [self presentViewController:viewController animated:YES completion:nil];
}

- (IBAction)googleSignIn:(id)sender {
    @try {
        GIDSignIn*sigNIn=[GIDSignIn sharedInstance];
        [sigNIn setDelegate:self];
        [sigNIn setUiDelegate:self];
        sigNIn.shouldFetchBasicProfile = YES;
        
        //        sigNIn.scopes = @[@"https://www.googleapis.com/auth/plus.login",@"https://www.googleapis.com/auth/userinfo.email",@"https://www.googleapis.com/auth/userinfo.profile"];
        
        [sigNIn signIn];
    } @catch (NSException *exception) {
        NSLog(@"Sign In With Google Error:%@",exception.description);
    }
    
}

- (IBAction)facebookSignIn:(id)sender {
    
    
    
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
             //             [[NSUserDefaults standardUserDefaults] setValue:@"no" forKey:@"isuserloggedin"];
             
             
         } else {
             NSLog(@"Logged in");
             NSString *accessToken = [FBSDKAccessToken currentAccessToken].tokenString;
             NSLog(@"FB Access Token:%@",accessToken);
             [[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:@"isuserloggedin"];
             
             
             if([[[NSUserDefaults standardUserDefaults] valueForKey:@"Sender"] isEqualToString:@"Home"])
             {
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     UIAlertController * alert2=   [UIAlertController
                                                    alertControllerWithTitle:@"Login Sucessful"
                                                    message:@""
                                                    preferredStyle:UIAlertControllerStyleAlert];
                     
                     [self presentViewController:alert2 animated:YES completion:nil];
                     
                     
                     
                     dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                         [alert2 dismissViewControllerAnimated:YES completion:nil];
                         [self.navigationController popViewControllerAnimated:YES];
                         [self.navigationController dismissViewControllerAnimated:NO completion:nil];
                     });
                     
                 });
                 
                 
             }
             
         }
     }];
    
}

- (IBAction)twitterSignIn:(id)sender {
    
    
    [[Twitter sharedInstance] logInWithMethods:TWTRLoginMethodWebBased completion:^(TWTRSession *session, NSError *error) {
        
        if (session) {
            NSLog(@"signed in as %@", [session userName]);
            
            [[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:@"isuserloggedin"];
            
            
            TWTRAPIClient *client = [TWTRAPIClient clientWithCurrentUser];
            NSURLRequest *request = [client URLRequestWithMethod:@"GET"
                                                             URL:@"https://api.twitter.com/1.1/account/verify_credentials.json"
                                                      parameters:@{@"include_email": @"true", @"skip_status": @"true"}
                                                           error:nil];
            
            [client sendTwitterRequest:request completion:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                
                NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                NSLog(@"Twitter Login Data:%@",dataString);
                if([[[NSUserDefaults standardUserDefaults] valueForKey:@"Sender"] isEqualToString:@"Home"])
                {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        UIAlertController * alert2=   [UIAlertController
                                                       alertControllerWithTitle:@"Login Sucessful"
                                                       message:@""
                                                       preferredStyle:UIAlertControllerStyleAlert];
                        
                        [self presentViewController:alert2 animated:YES completion:nil];
                        
                        
                        
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                            [alert2 dismissViewControllerAnimated:YES completion:nil];
                            [self.navigationController popViewControllerAnimated:YES];
                            [self.navigationController dismissViewControllerAnimated:NO completion:nil];
                        });
                        
                    });
                    
                    
                }
                
            }];
            
        }
        else {
            NSLog(@"error: %@", [error localizedDescription]);
            //            [[NSUserDefaults standardUserDefaults] setValue:@"no" forKey:@"isuserloggedin"];
            
        }
        
    }];
    
}

- (void)addUserInfoToRealm:(NSDictionary*)data {
    
    @try {
        
        
    } @catch (NSException *exception) {
        NSLog(@"Saving User Info Error:%@, Description:%@",exception,exception.description);
    } @finally {
        
    }
    
}
@end



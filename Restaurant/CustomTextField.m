//
//  CustomTextField.m
//  Restaurant
//
//  Created by HN on 03/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "CustomTextField.h"

@implementation CustomTextField


- (id)initWithCoder:(NSCoder*)coder {
   
    self = [super initWithCoder:coder];
    
    if (self) {
       
        //self.clipsToBounds = YES;
        //[self setRightViewMode:UITextFieldViewModeUnlessEditing];
        
//        self.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0,15,46)];
//        self.leftViewMode=UITextFieldViewModeAlways;
        self.borderStyle = UITextBorderStyleNone;
        //        self.background = [UIImage imageNamed:@"textfield.png"];
        
        CALayer *border = [CALayer layer];
        CGFloat borderWidth = 1.5;
        border.borderColor = [UIColor textFieldBorderColor].CGColor;
        border.frame = CGRectMake(0,self.frame.size.height - borderWidth , self.frame.size.width, self.frame.size.height);
        border.borderWidth = borderWidth;
        [self.layer addSublayer:border];
        self.layer.masksToBounds = YES;
        
        
//        if ([self.attributedPlaceholder length]) {
//            // Extract attributes
//            NSDictionary * attributes = (NSMutableDictionary *)[ (NSAttributedString *)self.attributedPlaceholder attributesAtIndex:0 effectiveRange:NULL];
//            
//            NSMutableDictionary * newAttributes = [[NSMutableDictionary alloc] initWithDictionary:attributes];
//            
//            [newAttributes setObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName];
//            
//            // Set new text with extracted attributes
//            self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[self.attributedPlaceholder string] attributes:newAttributes];
//            
//        }
    }
    
    return self;
    
}

@end

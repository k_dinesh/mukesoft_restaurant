//
//  RoleSelectionViewController.h
//  Restaurant
//
//  Created by HN on 28/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "AppDelegate.h"
#import "LocalData.h"
#import "AppLocationManager.h"
#import "Restaurants.h"
#import "RestaurantDetailsViewController.h"

@class LocalData;

@interface RoleSelectionViewController : UIViewController<AppLocationDelegate>
{
    AppLocationManager *locationController;
    
}


- (void)locationUpdate:(CLLocation *)location;
- (void)locationError:(NSError *)error;
@end

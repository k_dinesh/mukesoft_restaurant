//
//  AppUser.h
//  Restaurant
//
//  Created by HN on 31/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <Realm/Realm.h>

@interface AppUser : RLMObject
@property NSString *name;
@property NSString *email;
@property NSString *profilePic;
@property NSString *token;

@end

// This protocol enables typed collections. i.e.:
// RLMArray<AppUser>
RLM_ARRAY_TYPE(AppUser)

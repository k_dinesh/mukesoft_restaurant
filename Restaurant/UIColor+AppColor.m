//
//  UIColor+AppColor.m
//  Restaurant
//
//  Created by HN on 17/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "UIColor+AppColor.h"

@implementation UIColor (AppColor)
+ (UIColor *)appMainColor
{
    return [UIColor colorWithRed:252.0f/255.0f green:51.0f/255.0f blue:61.0f/255.0f alpha:1] ;
}

+ (UIColor *)textFieldBorderColor
{
    return [UIColor colorWithRed:255.0f/255.0f green:149.0f/255.0f blue:165.0f/255.0f alpha:1] ;
}
@end

//
//  ServerSearchCollectionFlowLayout.h
//  Restaurant
//
//  Created by HN on 19/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ServerSearchCollectionFlowLayout : UICollectionViewFlowLayout


@property (nonatomic,strong) NSMutableDictionary *layoutInfo;

@end

//
//  HomeViewController.h
//  Restaurant
//
//  Created by HN on 03/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <MapKit/MapKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "ServerHeaderView.h"
#import "AppDelegate.h"
#import "LoginView.h"
#import "AddServerViewController.h"
#import "ServerSearchViewController.h"
#import "ServerDetailViewController.h"
#import "MVPlaceSearchTextField/MVPlaceSearchTextField.h"


@interface HomeViewController : UIViewController <UITextFieldDelegate,UICollectionViewDataSource,PlaceSearchTextFieldDelegate,CLLocationManagerDelegate,GMSMapViewDelegate,UICollectionViewDelegateFlowLayout,UICollectionViewDelegate>
{
    BOOL *loggedInStatus;
    
}

@property (weak, nonatomic) IBOutlet UITextField *txtServerName;
@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *txtLocation;


@property (weak, nonatomic) IBOutlet UITextField *txtRestaurantName;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnLogin;
@property (weak, nonatomic) IBOutlet UICollectionView *serverListCollection;
@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;

- (IBAction)searchButtonPressed:(id)sender;


@end

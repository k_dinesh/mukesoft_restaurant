//
//  AddServerViewController.h
//  Restaurant
//
//  Created by HN on 17/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerDetailViewController.h"
#import "NewServer.h"
#import "MVPlaceSearchTextField.h"
#import "Servers.h"
#import "HomeViewController.h"


@interface AddServerViewController : UIViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate,PlaceSearchTextFieldDelegate,UITextFieldDelegate,UIAlertViewDelegate,UIGestureRecognizerDelegate>
{
    
    __weak IBOutlet UITextField *txtServerName;
    
    __weak IBOutlet MVPlaceSearchTextField *txLocation;
    
    __weak IBOutlet UITextField *txtRestaurantName;
    
    __weak IBOutlet UITextField *txtServerDesignation;
    
    __weak IBOutlet UIImageView *profilePic;
}



- (IBAction)addServerButtonPressed:(id)sender;

- (IBAction)addPhotoPressed:(id)sender;

- (IBAction)selectDesignation:(id)sender;




@end

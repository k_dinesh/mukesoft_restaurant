//
//  RestaurantDetailsViewController.m
//  Restaurant
//
//  Created by HN on 03/11/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "RestaurantDetailsViewController.h"

@interface RestaurantDetailsViewController ()
{
    Restaurants *newRestaurant;
    
}
@property(nonatomic,weak)RLMResults *restaurants;
@property (nonatomic, strong) RLMResults *ServerArray;
@end

@implementation RestaurantDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault]; //UIImageNamed:@"transparent.png"
    self.navigationController.navigationBar.shadowImage = [UIImage new];////UIImageNamed:@"transparent.png"
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    
    UIVisualEffect *blurEffect;
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    
    UIVisualEffectView *visualEffectView;
    visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    
    visualEffectView.frame = self.imgTitleBack.bounds;
    [self.imgTitleBack addSubview:visualEffectView];
    
    

    [self.collectionView setDelegate:self];
    [self.collectionView setDataSource:self];
     self.collectionView.collectionViewLayout = [[ServerSearchCollectionFlowLayout alloc] init];
    [self assignCollectionCellNib];
    
    NSLog(@"ServerID:%@",self.restaurantID);
    NSPredicate *predicate;
    predicate = [NSPredicate predicateWithFormat:@"serverID = %ld",[self.restaurantID integerValue]];
    newRestaurant = [[Restaurants objectsWithPredicate:predicate] firstObject];
    
    
    NSLog(@"Restaurant:%@",newRestaurant);

    
}

- (void)assignRestaurantInfo{
    
    self.lblRestaurantName.text = newRestaurant.name;
    self.lblCity.text = newRestaurant.city;
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,newRestaurant.imageURL]];
    
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    self.imgRestaurant.image = image;
                    
                });
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.imgRestaurant.backgroundColor = [UIColor lightGrayColor];
                });
            }
        }
    }];
    [task resume];

    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    [self assignCollectionCellNib];
}

- (void)assignCollectionCellNib {
    [self.collectionView registerNib:[UINib nibWithNibName:@"ServerSearchCollectionViewCell" bundle:[NSBundle mainBundle]]
          forCellWithReuseIdentifier:@"ServerSearchCollectionViewCell"];
    

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark Collectionview Delegates

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    
    
    return CGSizeMake(0., 50.);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    
        
    return self.ServerArray.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    ServerSearchCollectionViewCell *serverCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ServerSearchCollectionViewCell" forIndexPath:indexPath];
    
    
   /*
    Servers *newServer ;
    
    newServer = self.ServerArray[indexPath.row];
    
    
    // Assigning Name
    serverCell.serverName.text = newServer.serverName;
    
    // Assigning Image
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,newServer.imageURL]];
    
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    ServerSearchCollectionViewCell *updateCell = (id)[collectionView cellForItemAtIndexPath:indexPath];
                    if (updateCell)
                        updateCell.imageView.image = image;
                });
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    ServerSearchCollectionViewCell *updateCell = (id)[collectionView cellForItemAtIndexPath:indexPath];
                    if (updateCell)
                        updateCell.imageView.image = [UIImage imageNamed:@"ic_user_b.png"];;
                });
            }
        }
    }];
    [task resume];
    
    // Assigning Ratings
    
    if (newServer.toalRatings <= 0) {
        serverCell.ratings.text = [NSString stringWithFormat:@"0 Ratings"];
    }
    else
    {
        serverCell.ratings.text = [NSString stringWithFormat:@"%ld Ratings",newServer.toalRatings];
    }
    
    
    self.ratingStars = @[serverCell.star1,serverCell.star2,serverCell.star3,serverCell.star4,serverCell.star5];
    self.rating = newServer.toalRatings ;
    
    
    [self.ratingStars enumerateObjectsUsingBlock:^(UIImageView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj setImage:(idx < (NSInteger)newServer.avgRating ? [UIImage imageNamed:@"star_filled.png"] : [UIImage imageNamed:@"star_blank.png"])];
    }];
    
    // Assiginig Restaurant Name
    
    if (newServer.restaurantName.length > 0)
    {
        serverCell.restaurantName.text = newServer.restaurantName;
    }
    
    */
    
    
    return serverCell;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    if (self.ServerArray.count == 0) {
        UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
        NSInteger numberOfCells = self.view.frame.size.width / flowLayout.itemSize.width;
        NSInteger edgeInsets = (self.view.frame.size.width - (numberOfCells * flowLayout.itemSize.width)) / (numberOfCells + 1);
        return UIEdgeInsetsMake(0, edgeInsets, 0, edgeInsets);
    }
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView *headerCell;
    
    int count;
    
    
    count = (int)self.ServerArray.count;
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        ServerSearchHeaderView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"headerView" forIndexPath:indexPath];
        
        NSString *resultString;
        if(count <= 1)
        {
            resultString = @"Result Found";
        }
        else
        {
            resultString = @"Results Found";
        }
        header.lblHeaderTitle.text = [NSString stringWithFormat:@"%d %@",count,resultString];
        
        headerCell = header;
        
    }
    
    return headerCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    Servers *newServer = self.ServerArray[indexPath.row];
    
    ServerDetailViewController *serverDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ServerDetailViewController"];
    serverDetailVC.serverID = [NSString stringWithFormat:@"%ld",newServer.serverID];
    
    
    [self.navigationController pushViewController:serverDetailVC animated:YES];
    
    
    
}


@end

//
//  RatingSuccessAlertViewController.h
//  Restaurant
//
//  Created by HN on 20/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"



@protocol SuccessAlertDelegate<NSObject>
@required
- (void)dismissAlert;

@end



@interface RatingSuccessAlertViewController : UIViewController
{
    
    __weak IBOutlet UIView *alertBackground;
    
   
}

- (IBAction)closeButtonPressed:(id)sender;

- (IBAction)noButtonPressed:(id)sender;
- (IBAction)yesButtonPressed:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblAlertMessage;
@property (strong, nonatomic) IBOutlet UILabel *lblAlertTitle;

@property (strong,nonatomic) NSString *alertTitle;
@property (strong,nonatomic) NSString *alertMessage;

@property (nonatomic,weak) id<SuccessAlertDelegate> delegate;
@end


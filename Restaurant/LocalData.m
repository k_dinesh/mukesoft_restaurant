//
//  LocalData.m
//  Restaurant
//
//  Created by HN on 17/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "LocalData.h"

@implementation LocalData
@synthesize managedObjectContext = _managedObjectContext;



+ (instancetype)sharedInstance {
    static id _sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[[self class] alloc] init];
    });
    
    return _sharedInstance;
}

- (BOOL)loggedInStatus {
    
    _managedObjectContext = ((AppDelegate*)([UIApplication sharedApplication].delegate)).persistentContainer.viewContext;
    
    NSError *error = nil ;
    User *aUser = nil;
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    
    [request setEntity:[NSEntityDescription entityForName:@"User" inManagedObjectContext:_managedObjectContext]];
    
    
    /*------------ For Inserting New Object -----------------*/
    //    aUser = [NSEntityDescription insertNewObjectForEntityForName:@"User" inManagedObjectContext:_managedObjectContext];
    //    aUser.username = @"user1";
    //    aUser.isLoggenIn = NO;
    //
    /*-------------------------------------------------------*/
    
    //    NSArray *userArray = [_managedObjectContext executeFetchRequest:request error:&error];
    
    
    /*------------ For Updating existing Object -----------------*/
    aUser = [[_managedObjectContext executeFetchRequest:request error:&error] lastObject];
    /*-------------------------------------------------------*/
    NSLog(@"Username: %@",aUser.username);
    
    
    
    
    if(error) {
        NSLog(@"Unable to Save");
    }
    
    if (!aUser) {
        NSLog(@"No Entity");
    }
    
    //    aUser.isLoggenIn = YES;
    //    aUser.username = @"abc";
    //    aUser.password = @"123";
    //
    //    error = nil;
    //    if (![_managedObjectContext save:&error]) {
    //        NSLog(@"Error occured while saving data..");
    //    }
    //    else {
    //        NSLog(@"Data Save Successfully !!");
    //    }
    
    return aUser.isLoggenIn;
}

- (void)FetchAllServers:(void (^)(BOOL result))aResult {
    
    
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,GET_ALL_SERVERS_URL]];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        
        if (error) {
            NSLog(@"Error: %@", error);
            aResult(NO);
        } else {
            NSLog(@"%@ %@", response, responseObject);
            
            if ([responseObject isKindOfClass:[NSArray class]])
            {
                NSArray *jsonArray = (NSArray *)responseObject  ;
                NSLog(@"Json:%@",jsonArray);
            }
            
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *dataDic = (NSDictionary *)responseObject;
                //                NSLog(@"JsonDict:%@",dataDic);
                
                
                
                
                
                
                RLMRealm *realm = [RLMRealm defaultRealm];
                NSInteger count = 0;
                
                for (NSDictionary *servers in [dataDic valueForKey:@"data"]) {
                    
                    
                    [realm beginWriteTransaction];
                    
                    Servers *newServer = [[Servers alloc] init];
                    newServer.serverID = [[servers valueForKey:@"_id"] integerValue];
                    newServer.email = [servers valueForKey:@"email"];
                    newServer.city = [servers valueForKey:@"city"];
                    newServer.imageURL = [servers valueForKey:@"image"];
                    newServer.serverName =[servers valueForKey:@"name"];
                    newServer.avgRating = [[servers valueForKey:@"avgRating"] floatValue];
                    newServer.toalRatings = [[servers valueForKey:@"totalRatings"] floatValue];
                    newServer.workingRestroID = [[servers valueForKey:@"_idWorksInRestaurant"] integerValue];
                
                    [realm addObject:newServer];
                    [realm commitWriteTransaction];
                    
                    count ++;
                }
                
                
                NSLog(@"%ld Reconrds Saved",count);
                aResult(YES);
                
            }
            
            
        }
    }];
    [dataTask resume];
    
}

- (void)fetchRestaurantsAPI:(void (^)(BOOL result))aResult {
    [self clearPersistedData];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,GET_ALL_RESTAURANTS_URL]];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        if (error) {
            NSLog(@"Error: %@", error);
        } else {
            NSLog(@"%@ %@", response, responseObject);
            aResult(YES);
        }
    }];
    [dataTask resume];
    
}
- (void)FetchAllRestaurants:(void (^)(BOOL result))aResult {
    
    [self clearPersistedData];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,GET_ALL_RESTAURANTS_URL]];
    NSURLRequest *request = [NSURLRequest requestWithURL:URL];
    
    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        
        if (error) {
            NSLog(@"Error: %@", error.description);
            aResult(NO);
        } else {
            NSLog(@"%@ %@", response, responseObject);
            
            if ([responseObject isKindOfClass:[NSArray class]])
            {
                NSArray *jsonArray = (NSArray *)responseObject  ;
                NSLog(@"Json:%@",jsonArray);
            }
            
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                NSDictionary *dataDic = (NSDictionary *)responseObject;
                    NSLog(@"JsonDict:%@",dataDic);
                
                RLMRealm *realm = [RLMRealm defaultRealm];
                

                NSInteger count = 0;
                
                for (NSDictionary *restaurantsDict in [dataDic valueForKey:@"data"]) {
                    Restaurants *newRestaurant = [[Restaurants  alloc] init];
                    
                    
                    count ++;
                    
                    
                    [realm beginWriteTransaction];
                    
                    newRestaurant.restroID = [[restaurantsDict valueForKey:@"_id"] integerValue];
                    newRestaurant.city = [restaurantsDict valueForKey:@"city"];
                    newRestaurant.name = [restaurantsDict valueForKey:@"name"];
                    newRestaurant.ownerName = [restaurantsDict valueForKey:@"ownerName"];
                    newRestaurant.ownerEmail = [restaurantsDict valueForKey:@"ownerEmail"];
                    newRestaurant.imageURL = [restaurantsDict valueForKey:@"image"];
                    
                    
                    for (NSDictionary *serverData in [restaurantsDict valueForKey:@"servers"]) {
                        
                        if (serverData.count >0)
                        {
                            
                            Servers *newServer = [[Servers alloc] init];
                            newServer.serverID = [[serverData valueForKey:@"_id"] integerValue];
                            newServer.workingRestroID = [[serverData valueForKey:@"_idWorksInRestaurant"] integerValue];
                            newServer.avgRating = [[serverData valueForKey:@"avgRating"] floatValue];
                            newServer.imageURL = [serverData valueForKey:@"image"];
                            newServer.serverName = [serverData valueForKey:@"name"];
                            newServer.email = [serverData valueForKey:@"email"];
                            newServer.toalRatings = [[serverData valueForKey:@"totalRatings"] integerValue];
                            newServer.restaurantName = newRestaurant.name;
                            newServer.city = newRestaurant.city;
                             [realm addObject:newServer];
                        }
                    }
                    
                    NSDictionary *locations = [restaurantsDict valueForKey:@"location"];
                    
                    NSLog(@"Locations:%@",locations);
                    
                    newRestaurant.latitude = [locations valueForKey:@"lat"];
                    newRestaurant.longitude = [locations valueForKey:@"long"];
                   
                    [realm addObject:newRestaurant];
                    
                    
                    [realm commitWriteTransaction];
                    
                    
                }
                
                
                NSLog(@"%ld Reconrds Saved",count);
                aResult(YES);
                
            }
            
            
        }
    }];
    [dataTask resume];
    
}

- (void)rateServer:(NSString *)serverID
            rating:(NSInteger )aRate
        completion:(void (^)(BOOL success))completionBlock {
    
    
    
    
}

- (void)commentServer:(NSString *)serverID
              comment:(NSString * )aComment
           completion:(void (^)(BOOL success))completionBlock {
    
    
    NSMutableDictionary *dataDict = [NSMutableDictionary new];
    
    [dataDict setValue:serverID forKey:@"_idBartender"];
    [dataDict setValue:aComment forKey:@"comment"];
    
    
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataDict options:0 error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    
    
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@",SITE_URL,POST_COMMENT_URL] parameters:nil error:nil];
    
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        if (!error) {
            NSLog(@"Reply JSON: %@", responseObject);
            
            if ([responseObject isKindOfClass:[NSDictionary class]]) {
                
                
                completionBlock(YES);
            }
        } else {
            NSLog(@"Error: %@, %@, %@", error, response, responseObject);
            completionBlock(NO);
        }
    }] resume];
    
}

- (void)clearPersistedData {
    
    RLMRealm *realm = [RLMRealm defaultRealm];
    [realm beginWriteTransaction];
    [realm deleteObjects:[Servers allObjectsInRealm:realm]];
    [realm deleteObjects:[Restaurants allObjectsInRealm:realm]];
    [realm deleteObjects:[RestaurantImages allObjectsInRealm:realm]];
    [realm commitWriteTransaction];
    
    
}


@end

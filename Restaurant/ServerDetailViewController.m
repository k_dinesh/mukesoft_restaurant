//
//  ServerProfileViewController.m
//  Restaurant
//
//  Created by HN on 17/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "ServerDetailViewController.h"

@interface ServerDetailViewController ()
{
    __weak IBOutlet UIView *background1;
    
    __weak IBOutlet UIView *background2;
    
    __weak IBOutlet UIImageView *imgServerPic;
    
    Servers *newServer;
    AppUser *newUser;
    
    CGFloat animatedDistance;
    UIActivityIndicatorView *activityView;
}
@property (strong, nonatomic) NSArray<UIButton *> *buttonArray;
@property (nonatomic) NSInteger rating;

@property (strong, nonatomic) NSArray<UIImageView *> *imageViewArray;
@property (nonatomic) NSInteger displayRating;
@end

@implementation ServerDetailViewController
@synthesize starButton1 = _starButton1,starButton2 = _starButton2, starButton3 = _starButton3, starButton4 = _starButton4, starButton5 = _starButton5;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault]; //UIImageNamed:@"transparent.png"
    self.navigationController.navigationBar.shadowImage = [UIImage new];////UIImageNamed:@"transparent.png"
    self.navigationController.view.backgroundColor = [UIColor appMainColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
    
    activityView = [[UIActivityIndicatorView alloc]
                    initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    activityView.center=self.view.center;
    [activityView hidesWhenStopped];
    [self.view addSubview:activityView];
    
    [self.txtComment  setDelegate:self];
    
    
    
    
    UIVisualEffect *blurEffect;
    //    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleRegular];
    blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    
    UIVisualEffectView *visualEffectView;
    visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    
    visualEffectView.frame = _blurrImageview.bounds;
    [_blurrImageview addSubview:visualEffectView];
    
    self.serverProfilePic.layer.cornerRadius = self.serverProfilePic.frame.size.width /2 ;
    self.serverProfilePic.layer.masksToBounds = YES;
    
    //    self.navigationItem.hidesBackButton = YES;
    //    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(dissmissView:)];
    //
    //    self.navigationItem.leftBarButtonItem = backButton;
    
    background1.layer.cornerRadius = 5;
    background1.layer.masksToBounds = NO;
    background1.layer.shadowColor = [UIColor grayColor].CGColor;
    background1.layer.shadowOpacity = 0.8;
    background1.layer.shadowRadius = 2;
    background1.layer.shadowOffset = CGSizeMake(3.0f, 3.0f);
    
    background2.layer.cornerRadius = 5;
    background2.layer.masksToBounds = NO;
    background2.layer.shadowColor = [UIColor grayColor].CGColor;
    background2.layer.shadowOpacity = 0.8;
    background2.layer.shadowRadius = 2;
    background2.layer.shadowOffset = CGSizeMake(3.0f, 3.0f);
    
    
    imgServerPic.layer.cornerRadius = imgServerPic.frame.size.width / 2;
    
    self.buttonArray = @[self.starButton1,self.starButton2,self.starButton3,self.starButton4,self.starButton5];
    
    NSLog(@"ServerID:%@",self.serverID);
    NSPredicate *predicate;
    predicate = [NSPredicate predicateWithFormat:@"serverID = %ld",[self.serverID integerValue]];
    newServer = [[Servers objectsWithPredicate:predicate] firstObject];
    
    NSLog(@"User = %@",[[NSUserDefaults standardUserDefaults] valueForKey:@"token"]);
    NSLog(@"Server = %@",newServer);
    
    [self setServerInfo];
}
- (void)viewWillAppear:(BOOL)animated {
    
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"dismissalert"] isEqualToString:@"yes"]) {
     
        [[NSUserDefaults standardUserDefaults] setValue:@"no" forKey:@"dismissalert"];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void) viewDidAppear:(BOOL)animated {
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"dismissalert"] isEqualToString:@"yes"]) {
        
        [[NSUserDefaults standardUserDefaults] setValue:@"no" forKey:@"dismissalert"];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    
}

- (void) viewWillDisappear:(BOOL)animated {
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
}
- (void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)setServerInfo {
    
    
    // Assigning Name
    self.serverName.text = newServer.serverName;
    
    
    // Assigning Restaurant Name
    if (newServer.restaurantName.length <=0) {
        self.restaurantName.text = @"";
    }
    
    if((newServer.restaurantName.length > 0) && (newServer.city.length > 0))
    {
        self.restaurantName.text = [NSString stringWithFormat:@"%@,%@",newServer.restaurantName,newServer.city];
        
    }
    else if((newServer.restaurantName.length > 0 )&& (newServer.city.length <= 0)){
        self.restaurantName.text = newServer.restaurantName;
    }
    
    // Assigning Ratings
    if (newServer.toalRatings <=0){
        self.totalRatings.text = [NSString stringWithFormat:@"No Ratings"];
        
    }
    else{
        self.totalRatings.text = [NSString stringWithFormat:@"%ld Ratings",newServer.toalRatings];
    }
    
    self.imageViewArray = @[self.starImage1,self.starImage2,self.starImage3,self.starImage4,self.starImage5];
    self.displayRating = newServer.toalRatings;
    
    [self.imageViewArray enumerateObjectsUsingBlock:^(UIImageView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj setImage:(idx < (NSInteger)newServer.avgRating ? [UIImage imageNamed:@"star_filled.png"] : [UIImage imageNamed:@"star_blank.png"])];
    }];
    
    
    // Assigning Image
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,newServer.imageURL]];
    
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    self.serverProfilePic.image = image;
                    self.blurrImageview.image = image;
                });
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    self.serverProfilePic.image = [UIImage imageNamed:@"ic_user_b.png"];
                    self.blurrImageview.image = [UIImage imageNamed:@"ic_user_b.png"];
                });
            }
        }
    }];
    [task resume];
    
}
- (void)dissmissView:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)dismissAlert {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [[self view] endEditing:YES];
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    CGRect textFieldRect =
    [self.view.window convertRect:textField.bounds fromView:textField];
    CGRect viewRect =
    [self.view.window convertRect:self.view.bounds fromView:self.view];
    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    CGFloat numerator =
    midline - viewRect.origin.y
    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    CGFloat denominator =
    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    * viewRect.size.height;
    CGFloat heightFraction = numerator / denominator;
    if (heightFraction < 0.0)
    {
        heightFraction = 0.0;
    }
    else if (heightFraction > 1.0)
    {
        heightFraction = 1.0;
    }
    UIInterfaceOrientation orientation =
    [[UIApplication sharedApplication] statusBarOrientation];
    if (orientation == UIInterfaceOrientationPortrait ||
        orientation == UIInterfaceOrientationPortraitUpsideDown)
    {
        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    }
    else
    {
        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    }
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y -= animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    
    
    
}
- (void)textFieldDidEndEditing:(UITextField *)textfield {
    
    CGRect viewFrame = self.view.frame;
    viewFrame.origin.y += animatedDistance;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    
    [self.view setFrame:viewFrame];
    
    [UIView commitAnimations];
    [self.view endEditing:YES];
    
}
- (BOOL)textFieldShouldReturn:(UITextField*)textField {
    [textField resignFirstResponder];
    return YES;
}


- (IBAction)postCommentButtonPressed:(UIButton *)sender {
    
    
    
    
    [self.view endEditing:YES];
    [_txtComment resignFirstResponder];
    
    if(_txtComment.text.length <=0)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Please Enter Comment"
                                           message:@""
                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:YES completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [alert2 dismissViewControllerAnimated:YES completion:nil];
            });
        });
        return;
        
    }
    
    
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"isuserloggedin"] isEqualToString:@"no"]) {
        
        
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Authentication Required"
                                           message:@"For posting comment User Authentication required"
                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:YES completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [alert2 dismissViewControllerAnimated:YES completion:nil];
            });
        });
    }
    else {
        
        
        
        
        
        RatingSuccessAlertViewController   *ratingAlert = [self.storyboard instantiateViewControllerWithIdentifier:@"RatingSuccessAlertViewController"];
        ratingAlert.alertTitle = @"Thank You for Commenting";
        ratingAlert.alertMessage = @"Would you like to post Comment on other servers?";
        ratingAlert.transitioningDelegate = (id)self;
        ratingAlert.modalPresentationStyle = UIModalPresentationCustom;
        
        
        
        
        [self.navigationController presentViewController:ratingAlert animated:YES completion:nil];
        
        
        
        /*
         
         {
         NSMutableDictionary *dataDict = [NSMutableDictionary new];
         
         [dataDict setValue:[NSString stringWithFormat:@"%ld",(long)newServer.serverID] forKey:@"_idBartender"];
         [dataDict setValue:_txtComment.text forKey:@"comment"];
         
         
         NSError *error;
         NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataDict options:0 error:&error];
         NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
         
         AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
         
         NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@",SITE_URL,POST_COMMENT_URL] parameters:nil error:nil];
         
         
         NSString *token = [[NSUserDefaults standardUserDefaults] objectForKey:@"token"];
         
         NSLog(@"Token:%@",token);
         
         
         
         [req setValue:[NSString stringWithFormat:@"bearer %@",token] forHTTPHeaderField:@"Authorization"];
         
         [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
         
         [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse *response, id data, NSError *error) {
         
         //        NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
         
         NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
         double status = (long)[httpResponse statusCode];
         NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
         
         
         
         if ([data isKindOfClass:[NSDictionary class]])
         
         {
         
         NSDictionary *dataDic = (NSDictionary *)data;
         NSLog(@"Data:%@",dataDic);
         
         NSInteger status = [[dataDic valueForKey:@"status"] integerValue];
         if(status == 202)
         {
         
         [activityView stopAnimating];
         
         RatingSuccessAlertViewController   *ratingAlert = [self.storyboard instantiateViewControllerWithIdentifier:@"RatingSuccessAlertViewController"];
         
         ratingAlert.transitioningDelegate = (id)self;
         ratingAlert.modalPresentationStyle = UIModalPresentationCustom;
         
         
         [self.navigationController presentViewController:ratingAlert animated:YES completion:nil];
         
         }
         else if(status == 400)
         {
         [activityView stopAnimating];
         
         dispatch_async(dispatch_get_main_queue(), ^{
         
         UIAlertController * alert2=   [UIAlertController
         alertControllerWithTitle:@"Sorry"
         message:[dataDic valueForKey:@"result"]
         preferredStyle:UIAlertControllerStyleAlert];
         
         [self presentViewController:alert2 animated:YES completion:nil];
         
         
         
         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
         [alert2 dismissViewControllerAnimated:YES completion:nil];
         });
         });
         
         }
         
         }
         
         
         }] resume];
         
         }
         */
        
        _txtComment.text = @"";
    }
    
    
}

- (IBAction)starRatingPressed:(UIButton *)sender {
    
    [activityView startAnimating];
    RLMRealm *realm = [RLMRealm defaultRealm];
    
    NSInteger buttonIndex = [self.buttonArray indexOfObject:sender];
    if (buttonIndex == NSNotFound) {   return;    }
    
    self.rating = buttonIndex +1;
    {
        
        NSMutableDictionary *dataDict = [NSMutableDictionary new];
        
        [dataDict setValue:[NSString stringWithFormat:@"%ld",(long)newServer.serverID] forKey:@"_idBartender"];
        [dataDict setValue:[NSString stringWithFormat:@"%ld", (long)self.rating] forKey:@"rating"];
        
        
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dataDict options:0 error:&error];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        
        NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@%@",SITE_URL,POST_RATING_URL] parameters:nil error:nil];
        
        [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
        
        [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse *response, id data, NSError *error) {
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
//            double status = (long)[httpResponse statusCode];
            NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
            
            if ([data isKindOfClass:[NSDictionary class]]){
                
                NSDictionary *dataDic = (NSDictionary *)data;
                NSLog(@"Data:%@",dataDic);
                
                NSInteger status = [[dataDic valueForKey:@"status"] integerValue];
                if(status == 202)
                {
                    [self.buttonArray enumerateObjectsUsingBlock:^(UIButton * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                        //        [obj setTitle:(idx <= buttonIndex ? @"★" : @"☆") forState:UIControlStateNormal];
                        [obj setImage:(idx <= buttonIndex ? [UIImage imageNamed:@"star_big_red.png"] : [UIImage imageNamed:@"star_big_gray.png"]) forState:UIControlStateNormal];
                    }];
                    
                    [activityView stopAnimating];
                    NSDictionary  *serverData;
                    
                    NSPredicate *predicate;
                    
                    [realm beginWriteTransaction];
                    
                    for (serverData in [dataDic valueForKey:@"data"]) {
                        
                        
                    }
                    predicate = [NSPredicate predicateWithFormat:@"serverID = %ld",[[serverData valueForKey:@"_idUserProfile"] integerValue]];
                    Servers *server =  [[Servers objectsWithPredicate:predicate] firstObject];
                    
                    
                    server.toalRatings = [[serverData valueForKey:@"totalRatings"] integerValue];
                    server.avgRating = [[serverData valueForKey:@"avgRating"] floatValue];
                    
                    [realm addObject:server];
                    [realm commitWriteTransaction];
                    
                    RatingSuccessAlertViewController   *ratingAlert = [self.storyboard instantiateViewControllerWithIdentifier:@"RatingSuccessAlertViewController"];
                    
                    
                    ratingAlert.alertTitle = @"Thank You for Rating";
                    ratingAlert.alertMessage = @"Would you like to rate other servers";
                    ratingAlert.delegate = self;
                    
                    ratingAlert.transitioningDelegate = (id)self;
                    ratingAlert.modalPresentationStyle = UIModalPresentationCustom;
                    
                    
                    [self.navigationController presentViewController:ratingAlert animated:YES completion:nil];
                    
                    
                }
                else if(status == 400)
                {
                    [activityView stopAnimating];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        UIAlertController * alert2=   [UIAlertController
                                                       alertControllerWithTitle:@"Sorry"
                                                       message:[dataDic valueForKey:@"result"]
                                                       preferredStyle:UIAlertControllerStyleAlert];
                        
                        [self presentViewController:alert2 animated:YES completion:nil];
                        
                        
                        
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                            [alert2 dismissViewControllerAnimated:YES completion:nil];
                        });
                    });
                    
                }
                
            }
            
            
        }] resume];
        
    }
    
}


@end

//
//  HomeViewController.m
//  Restaurant
//
//  Created by HN on 03/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "HomeViewController.h"
#import "ServerListCell.h"
#import "ServerListCustomFlowLayout.h"




@interface HomeViewController ()
{
    RLMResults *collectionViewRestaurantArray;
    RLMResults *collectionViewRestaurantImageArray;
    RLMResults *collectionViewServerArray;
    
    Restaurants *restaurant;
    RestaurantImages *restaurantImage;
    Servers *server;
    CLLocationCoordinate2D fromLocationCoord;
    CLLocationCoordinate2D toLocationCoord;
    NSString *fromLocationString;
    NSString *toLocationString;

    CALayer *border;
    CGFloat borderWidth ;
    
}


@property (strong, nonatomic) NSArray<UIImageView *> *ratingStars;
@property (nonatomic) NSInteger rating;

@property (nonatomic, strong) RLMResults *ServerArray;
@property (nonatomic, strong) RLMResults *RestaurantArray;


#define kHeaderIdentifier @"header"

@property(nonatomic, copy) NSAttributedString *attributedPlaceholder;

@end

@implementation HomeViewController


- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:YES];
    [self.collectionView registerNib:[UINib nibWithNibName:@"ServerListCell" bundle:[NSBundle mainBundle]]
          forCellWithReuseIdentifier:@"ServerListCell"];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [self.navigationController.navigationBar setBackgroundColor:[UIColor clearColor]];
    [self updateData];
    [self setLoggedInUser];
}

- (void) viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault]; //UIImageNamed:@"transparent.png"
    self.navigationController.navigationBar.shadowImage = [UIImage new];////UIImageNamed:@"transparent.png"
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    [_txtServerName setDelegate:self];
    [_txtLocation  setDelegate:self];
    [_txtRestaurantName setDelegate:self];
    
    
    
    [self.collectionView setDelegate:self];
    [self.collectionView setDataSource:self];
    
    self.collectionView.collectionViewLayout = [[ServerListCustomFlowLayout alloc] init];
    
    NSLog(@"Server Data:%@",collectionViewServerArray);
    
//    NSLog(@"Server Count: %lu",(unsigned long)collectionViewServerArray.count);
  
    AppUser *user = [[AppUser alloc] init];
    
    NSLog(@"Token:%@",user.token);
    
    
    [self setChangeLocationButton];
    [self setupAutoCompleteTextField];
    [self customTextField];
    [self updateData];
    [self setLoggedInUser];
    
}

- (void) customTextField {
    
    _txtLocation.borderStyle = UITextBorderStyleNone;
    
    border = [CALayer layer];
    borderWidth = 1.5;
    border.borderColor = [UIColor textFieldBorderColor].CGColor;
    border.frame = CGRectMake(0,_txtLocation.frame.size.height - borderWidth , _txtLocation.frame.size.width, _txtLocation.frame.size.height);
    border.borderWidth = borderWidth;
    [_txtLocation.layer addSublayer:border];
    _txtLocation.layer.masksToBounds = YES;
}

#pragma mark - Set up additional data and ui
- (void) setChangeLocationButton {
    UIButton* overlayButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [overlayButton setTitle:@"Change Location" forState:UIControlStateNormal];
    [overlayButton addTarget:self action:@selector(changeLocationPressed:)
            forControlEvents:UIControlEventTouchUpInside];
    [overlayButton setFrame:CGRectMake(0, 0, _txtLocation.frame.size.width/2.5, 28)];
    overlayButton.titleLabel.font = [UIFont systemFontOfSize:14];
    
    overlayButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    
    // Assign the overlay button to a stored text field
    self.txtLocation.rightView = overlayButton;
    self.txtLocation.rightViewMode = UITextFieldViewModeAlways;
}
- (void) updateData {
    
    [self fetchDataFromServer];
    
    }
- (void) setLoggedInUser {
    
//    NSString *flag = [[NSUserDefaults standardUserDefaults] valueForKey:@"isuserloggedin"];
//    NSLog(@"flag:%@",flag);
//    if([flag isEqualToString:@"yes"] ){
//        
//        NSString *title = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
//        NSLog(@"Title:%@",title);
//        [self.btnLogin setTitle:title];
    
//    }
}
- (void) changeLocationPressed:(id)sender {
    NSLog(@"Change Location");
    _txtLocation.text = @"";
    
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:YES];
}

- (void) changePlaceHolderColor {
    if ([self.attributedPlaceholder length]) {
        // Extract attributes
        NSDictionary * attributes = (NSMutableDictionary *)[ (NSAttributedString *)self.attributedPlaceholder attributesAtIndex:0 effectiveRange:NULL];
        
        NSMutableDictionary * newAttributes = [[NSMutableDictionary alloc] initWithDictionary:attributes];
        
        [newAttributes setObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName];
        
        // Set new text with extracted attributes
        self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[self.attributedPlaceholder string] attributes:newAttributes];
        
    }
    
    
}
- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Fetch Data From Server
- (void) fetchDataFromServer {
    
    
    UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc]
                                             initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    activityView.center=self.view.center;
    [activityView startAnimating];
    [activityView hidesWhenStopped];
    [self.view addSubview:activityView];

    if ([self isNetworkAvailable]) {
        
        [[LocalData sharedInstance] FetchAllRestaurants:^(BOOL result) {
            if (result)
            {
                NSLog(@"Restaurant Data Downloaded");
                
                collectionViewRestaurantArray = [Restaurants allObjects];
                collectionViewRestaurantImageArray = [RestaurantImages allObjects];
                collectionViewServerArray = [Servers allObjects];
                self.ServerArray = [[Servers allObjects]sortedResultsUsingProperty:@"toalRatings" ascending:NO];
                
                NSLog(@"Servers:%@",_ServerArray);
                AppUser *user;
                
                NSLog(@"Token:%@",user.token);
                [self.collectionView reloadData];

                [activityView stopAnimating];
            }
            else{
                [activityView stopAnimating];
            }
        }];
        
//        [self initLocationManager];
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Please Check Internet Connectivity"
                                           message:@""
                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:YES completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                
                [alert2 dismissViewControllerAnimated:YES completion:nil];
                return ;
                
            });
            
        });
    }
    
}

#pragma mark - collectionView methods
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(0., 50.);
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    return self.ServerArray.count;

}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    Servers *newServer = self.ServerArray[indexPath.row];
    
       ServerListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ServerListCell" forIndexPath:indexPath];
    
   // Assigning Name
    cell.serverName.text = newServer.serverName;
    
    // Assigning Image
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,newServer.imageURL]];
    
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    ServerListCell *updateCell = (id)[collectionView cellForItemAtIndexPath:indexPath];
                    if (updateCell)
                        updateCell.imageView.image = image;
                });
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    ServerListCell *updateCell = (id)[collectionView cellForItemAtIndexPath:indexPath];
                    if (updateCell)
                        updateCell.imageView.image = [UIImage imageNamed:@"ic_user_b.png"];;
                });
            }
        }
    }];
    [task resume];
    
    // Assigning Ratings
    
    if (newServer.toalRatings <= 0) {
    cell.ratings.text = [NSString stringWithFormat:@"0 Ratings"];
    }
    else
    {
    cell.ratings.text = [NSString stringWithFormat:@"%ld Ratings",newServer.toalRatings];
    }
    
    
    self.ratingStars = @[cell.star1,cell.star2,cell.star3,cell.star4,cell.star5];
    self.rating = newServer.toalRatings ;

        
        [self.ratingStars enumerateObjectsUsingBlock:^(UIImageView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [obj setImage:(idx < (NSInteger)newServer.avgRating ? [UIImage imageNamed:@"star_filled.png"] : [UIImage imageNamed:@"star_blank.png"])];
        }];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    Servers *newServer = self.ServerArray[indexPath.row];
    
    ServerDetailViewController *serverDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ServerDetailViewController"];
    serverDetailVC.serverID = [NSString stringWithFormat:@"%ld",newServer.serverID];
    
    
    [self.navigationController pushViewController:serverDetailVC animated:YES];
    
    
    
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView *headerCell;
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        ServerHeaderView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"headerView" forIndexPath:indexPath];
        header.lblHeaderTitle.text = @"Top Server List";
        
        headerCell = header;
        
    }
    
    return headerCell;
}

- (void)updateSectionHeader:(UICollectionReusableView *)header forIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark Search Funtion Action and Implementation
- (IBAction)searchButtonPressed:(id)sender {
    
    
    
    NSString *location;
    if (![_txtLocation.text isEqualToString:@""]) {
        
     location = [self validateAddress:_txtLocation.text];
        
        
    }
    NSLog(@"Filtered Location:%@",location);
    if ((location.length <= 0) && (![_txtLocation.text isEqualToString:@""]))
    {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@""
                                           message:@"Please Enter Valid Location"
                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:YES completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [alert2 dismissViewControllerAnimated:YES completion:nil];
                
            });
            return ;
        });
        
    }
    else if ((_txtServerName.text.length <= 0) && (_txtRestaurantName.text.length <= 0)) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Please Enter Information"
                                           message:@"Server Name or Restaurant Name required for search result "
                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:YES completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [alert2 dismissViewControllerAnimated:YES completion:nil];
                
            });
            return ;
        });
        

    }
    else {
        ServerSearchViewController *searchServer = [self.storyboard instantiateViewControllerWithIdentifier:@"ServerSearchViewController"];
        searchServer.serverName = _txtServerName.text;
        searchServer.location = location;
        searchServer.restaurantName = _txtRestaurantName.text;
        
        [self clearTextfileds];
        
        [self.navigationController pushViewController:searchServer animated:YES];
        
    }
}

- (IBAction)loginButtonPressed:(id)sender {
    [self clearTextfileds];
    
        LoginView *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
        [self.navigationController pushViewController:login animated:YES];
        
        [[NSUserDefaults standardUserDefaults] setValue:@"Home" forKey:@"Sender"];
    
    
}

- (void)clearTextfileds {
    [self.view endEditing:YES];
    
    _txtServerName.text = @"";
    _txtLocation.text = @"";
    _txtRestaurantName.text = @"";
    
}

#pragma mark - Place search Textfield Delegates

-(void)setupAutoCompleteTextField {
    
    _txtLocation.placeSearchDelegate = self;
    
    _txtLocation.strApiKey = kGoogleAPIKey;
    
    _txtLocation.superViewOfList = self.view;
    
    _txtLocation.autoCompleteShouldHideOnSelection = YES;
    
    _txtLocation.maximumNumberOfAutoCompleteRows  = 10;
    
    
}

-(void)placeSearch:(MVPlaceSearchTextField*)textField ResponseForSelectedPlace:(GMSPlace*)responseDict {
    [self.view endEditing:YES];
    [_txtLocation resignFirstResponder];
    
        NSLog(@"SELECTED ADDRESS :%@",responseDict);
    //    NSLog(@"Co-ordinate:(%f,%f)",responseDict.coordinate.latitude,responseDict.coordinate.longitude);
    //    NSLog(@"Address:%@",responseDict.formattedAddress);
    
    
    if (textField == _txtLocation) {
        fromLocationCoord.latitude = responseDict.coordinate.latitude;
        fromLocationCoord.longitude = responseDict.coordinate.longitude;
        //        fromLocationString = responseDict.formattedAddress;
        
        
    }
    
}
-(void)placeSearchWillShowResult:(MVPlaceSearchTextField*)textField {
    
}
-(void)placeSearchWillHideResult:(MVPlaceSearchTextField*)textField {
    
}
-(void)placeSearch:(MVPlaceSearchTextField*)textField ResultCell:(UITableViewCell*)cell withPlaceObject:(PlaceObject*)placeObject atIndex:(NSInteger)index {
    if(index%2==0){
        cell.contentView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    }else{
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
}


- (NSString*)validateAddress:(NSString*)addressString {
    
    
    @try {
        
        
        NSCharacterSet *set = [NSCharacterSet URLHostAllowedCharacterSet];
        NSString *resultString = [addressString stringByAddingPercentEncodingWithAllowedCharacters:set];
        
        NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", resultString];
        NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
        
        NSError *jsonError;
        NSData *objectData = [result dataUsingEncoding:NSUTF8StringEncoding];
        
        NSDictionary *jsonArray=[NSJSONSerialization JSONObjectWithData:objectData options:-1 error:nil];
        
        NSArray *addressArray=[[jsonArray valueForKeyPath:@"results.address_components"] objectAtIndex:0];
        
        
        if([[jsonArray valueForKey:@"status"] isEqualToString:@"OK"])
        {
            //        NSString *state;
            NSString *city;
            
            for (NSDictionary *dictAddress in addressArray)
            {
                //            if ([[[dictAddress objectForKey:@"types"]objectAtIndex:0] isEqualToString:@"administrative_area_level_1"])
                //                {
                //                    state = [dictAddress objectForKey:@"long_name"];
                //                     NSLog(@"state :%@",state);
                //
                //                }
                
                city = [dictAddress objectForKey:@"long_name"];
                
                return city;
                if ([[[dictAddress objectForKey:@"types"]objectAtIndex:0] isEqualToString:@"country"]) {
                    
                    
                }
                
            }
        }
        
        return @"New York";
    } @catch (NSException *exception) {
        NSLog(@"%@",exception);
        return nil;
    }
    
}

- (bool)isNetworkAvailable {
    SCNetworkReachabilityFlags flags;
    SCNetworkReachabilityRef address;
    address = SCNetworkReachabilityCreateWithName(NULL, "www.apple.com" );
    Boolean success = SCNetworkReachabilityGetFlags(address, &flags);
    CFRelease(address);
    
    bool canReach = success
    && !(flags & kSCNetworkReachabilityFlagsConnectionRequired)
    && (flags & kSCNetworkReachabilityFlagsReachable);
    
    return canReach;
}
@end

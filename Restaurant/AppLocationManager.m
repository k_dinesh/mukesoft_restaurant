//
//  AppLocationManager.m
//  Restaurant
//
//  Created by HN on 29/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "AppLocationManager.h"

@implementation AppLocationManager

@synthesize locationManager;
@synthesize delegate;

- (id) init {
    self = [super init];
  
        self.locationManager = [[CLLocationManager alloc] init] ;
        self.locationManager.delegate = self; // send loc updates to myself
      return self;
}

- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    
//    NSLog(@"Location: %@", [newLocation description]);
    [self.delegate locationUpdate:newLocation];
    
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
//    NSLog(@"Error: %@", [error description]);
    [self.delegate locationError:error];
    
}

@end

//
//  ServerSearchViewController.m
//  Restaurant
//
//  Created by HN on 18/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "ServerSearchViewController.h"
#import "ServerListCustomFlowLayout.h"

@interface ServerSearchViewController ()

@property (nonatomic, strong) RLMResults *ServerArray;
@property (nonatomic, strong) RLMResults *filteredServerArray;
@property  BOOL isFiltered;

@property (strong, nonatomic) NSArray<UIImageView *> *ratingStars;
@property (nonatomic) NSInteger rating;

@end


@implementation ServerSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationbar];
    
    NSLog(@"Server:%@, City:%@, Restaurant:%@:",self.serverName,self.location,self.restaurantName);
    
    
    @try {
        
        NSPredicate *predicate;
        
        // Filter by server
        if ((self.serverName.length > 0) && (self.location.length <= 0) && (self.restaurantName.length <= 0)) {
            predicate = [NSPredicate predicateWithFormat:@"serverName CONTAINS[c] %@",self.serverName];
            self.ServerArray = [Servers objectsWithPredicate:predicate];
        }
        
        // Filter by location
        if ((self.serverName.length <= 0) && (self.location.length > 0) && (self.restaurantName.length <= 0)) {
            predicate = [NSPredicate predicateWithFormat:@"city CONTAINS[c] %@",self.location];
            self.ServerArray = [Servers objectsWithPredicate:predicate];
        }
        
        // Filter by restaurant
        if ((self.serverName.length <= 0) && (self.location.length <= 0) && (self.restaurantName.length > 0)) {
            predicate = [NSPredicate predicateWithFormat:@"restaurantName CONTAINS[c] %@",self.restaurantName];
            self.ServerArray = [Servers objectsWithPredicate:predicate];
        }
        
        // Filter By Server and location
        if ((self.serverName.length > 0) && (self.location.length > 0)) {
            predicate = [NSPredicate predicateWithFormat:@"serverName CONTAINS[c] %@ AND city CONTAINS[c] %@",self.serverName,self.location];
            self.ServerArray = [Servers objectsWithPredicate:predicate];
        }
        
        // Filter By location and Restaurant
        if ((self.location.length > 0) && (self.restaurantName.length > 0)) {
            predicate = [NSPredicate predicateWithFormat:@"serverName CONTAINS[c] %@ AND restaurantName CONTAINS[c] %@",self.serverName,self.restaurantName];
            self.ServerArray = [Servers objectsWithPredicate:predicate];
        }
        
        // Filter By Server and Restaurant
        if ((self.serverName.length > 0) && (self.restaurantName.length > 0)) {
            predicate = [NSPredicate predicateWithFormat:@"serverName CONTAINS[c] %@ AND restaurantName CONTAINS[c] %@",self.serverName,self.restaurantName];
            self.ServerArray = [Servers objectsWithPredicate:predicate];
        }

        
    } @catch (NSException *exception) {
        NSLog(@"Exception:%@",exception.description);
    } @finally {
        
//        self.ServerArray = [Servers allObjects];
    }
    
    NSLog(@"Searched Server:%@",_ServerArray);
    self.filteredServerArray = self.ServerArray;
    
    [_txtServerName setDelegate:self];
    [_txtLocation  setDelegate:self];
    [_txtRestaurantName setDelegate:self];
    
    _txtServerName.text = self.serverName;
    _txtLocation.text = self.location;
    _txtRestaurantName.text = self.restaurantName;
    
    [self.collectionView setDelegate:self];
    [self.collectionView setDataSource:self];
    
    
    if (self.ServerArray.count > 0) {
        self.collectionView.collectionViewLayout = [[ServerSearchCollectionFlowLayout alloc] init];
    }
    else{
        self.collectionView.collectionViewLayout = [[ServerSearchEmptyCellFlowLayout alloc] init];
    }
    
    
    [_txtServerName addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_txtLocation addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
    [_txtRestaurantName addTarget:self action:@selector(textDidChange:) forControlEvents:UIControlEventEditingChanged];
  
    [self setLoggedInUser];
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:YES];
    //        self.navigationController.view.backgroundColor = [UIColor appMainColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];

}
- (void) viewWillAppear:(BOOL)animated  {
    [super viewWillAppear:YES];
    [self.collectionView registerNib:[UINib nibWithNibName:@"ServerSearchCollectionViewCell" bundle:[NSBundle mainBundle]]
          forCellWithReuseIdentifier:@"ServerSearchCollectionViewCell"];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"ServerSearchEmptyCollectionViewCell" bundle:[NSBundle mainBundle]]
          forCellWithReuseIdentifier:@"ServerSearchEmptyCollectionViewCell"];
   [self setNavigationbar];
    [self setLoggedInUser];
}

- (void) setLoggedInUser {
    
    
    
    NSString *flag = [[NSUserDefaults standardUserDefaults] valueForKey:@"isuserloggedin"];
    NSLog(@"flag:%@",flag);
    if([flag isEqualToString:@"yes"] ){
        
        NSString *title = [[NSUserDefaults standardUserDefaults] valueForKey:@"username"];
        NSLog(@"Title:%@",title);
        [self.btnLogin setTitle:title];
        
    }
}
- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textDidChange:(id)sender {

    UITextField* searchField = (UITextField *) sender;
    
    if (searchField == _txtServerName) {
        if(searchField.text.length == 0)
        {
            self.isFiltered = FALSE;
            
        }
        else {
            self.isFiltered = TRUE;
            
            NSPredicate *predicate;
            
            predicate = [NSPredicate predicateWithFormat:@"serverName CONTAINS[c] %@",searchField.text];
            self.filteredServerArray = [Servers objectsWithPredicate:predicate];
            NSLog(@"Filtered Data:%@",self.filteredServerArray);
            [self.collectionView reloadData];
        }
    }
    if (searchField == _txtLocation) {
        if(searchField.text.length == 0)
        {
            self.isFiltered = FALSE;
            
        }
        else {
            self.isFiltered = TRUE;
            
            NSPredicate *predicate;
            
            predicate = [NSPredicate predicateWithFormat:@"city CONTAINS[c] %@",searchField.text];
            self.filteredServerArray = [Servers objectsWithPredicate:predicate];
            NSLog(@"Filtered Data:%@",self.filteredServerArray);
            [self.collectionView reloadData];
        }

    }
    if(searchField == _txtRestaurantName) {
        
        if(searchField.text.length == 0)
        {
            self.isFiltered = FALSE;
            
        }
        else {
            self.isFiltered = TRUE;
            
            NSPredicate *predicate;
            
            predicate = [NSPredicate predicateWithFormat:@"restaurantName CONTAINS[c] %@",searchField.text];
            self.filteredServerArray = [Servers objectsWithPredicate:predicate];
            NSLog(@"Filtered Data:%@",self.filteredServerArray);
            [self.collectionView reloadData];
        }
        
    }
    
    

    
}
- (void)setNavigationbar {
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault]; //UIImageNamed:@"transparent.png"
    self.navigationController.navigationBar.shadowImage = [UIImage new];////UIImageNamed:@"transparent.png"
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    [self.navigationController.navigationBar setBarTintColor:self.view.backgroundColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(0, -60)
                                                         forBarMetrics:UIBarMetricsDefault];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


#pragma mark Collectionview Delegate Methods

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
   
    
    return CGSizeMake(0., 50.);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    
    if (self.ServerArray.count == 0) {
        return 1;
    }
    
    if (self.isFiltered)
    {
        return self.filteredServerArray.count;
    }
    
    if (self.filteredServerArray.count == 0){
        
        return 1;
    }
    
    return self.ServerArray.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if(self.ServerArray.count == 0 && self.filteredServerArray.count == 0)
    {
        ServerSearchEmptyCollectionViewCell *emptyCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ServerSearchEmptyCollectionViewCell" forIndexPath:indexPath];
        
        [emptyCell.addServerButton addTarget:self action:@selector(addNewServerPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        return emptyCell;
        
    }
    
    Servers *newServer ;
    
    if (self.isFiltered) {
        
        newServer = self.filteredServerArray[indexPath.row];
    }
    else{
        newServer = self.ServerArray[indexPath.row];
    }
   
        ServerSearchCollectionViewCell *serverCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ServerSearchCollectionViewCell" forIndexPath:indexPath];
    
    // Assigning Name
    serverCell.serverName.text = newServer.serverName;
    
    // Assigning Image
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,newServer.imageURL]];
    
    NSURLSessionTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (data) {
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    ServerSearchCollectionViewCell *updateCell = (id)[collectionView cellForItemAtIndexPath:indexPath];
                    if (updateCell)
                        updateCell.imageView.image = image;
                });
            }
            else {
                dispatch_async(dispatch_get_main_queue(), ^{
                    ServerSearchCollectionViewCell *updateCell = (id)[collectionView cellForItemAtIndexPath:indexPath];
                    if (updateCell)
                        updateCell.imageView.image = [UIImage imageNamed:@"ic_user_b.png"];;
                });
            }
        }
    }];
    [task resume];
    
    // Assigning Ratings
    
    if (newServer.toalRatings <= 0) {
        serverCell.ratings.text = [NSString stringWithFormat:@"0 Ratings"];
    }
    else
    {
        serverCell.ratings.text = [NSString stringWithFormat:@"%ld Ratings",newServer.toalRatings];
    }
    
    
    self.ratingStars = @[serverCell.star1,serverCell.star2,serverCell.star3,serverCell.star4,serverCell.star5];
    self.rating = newServer.toalRatings ;
    
    
    [self.ratingStars enumerateObjectsUsingBlock:^(UIImageView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj setImage:(idx < (NSInteger)newServer.avgRating ? [UIImage imageNamed:@"star_filled.png"] : [UIImage imageNamed:@"star_blank.png"])];
    }];
    
    // Assiginig Restaurant Name
    
    if (newServer.restaurantName.length > 0)
    {
    serverCell.restaurantName.text = newServer.restaurantName;
    }

    return serverCell;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    if (self.ServerArray.count == 0) {
        UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
        NSInteger numberOfCells = self.view.frame.size.width / flowLayout.itemSize.width;
        NSInteger edgeInsets = (self.view.frame.size.width - (numberOfCells * flowLayout.itemSize.width)) / (numberOfCells + 1);
        return UIEdgeInsetsMake(0, edgeInsets, 0, edgeInsets);
    }
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView
           viewForSupplementaryElementOfKind:(NSString *)kind
                                 atIndexPath:(NSIndexPath *)indexPath {
    UICollectionReusableView *headerCell;
    
    int count;
    
    if (self.isFiltered) {
        
        count = (int)self.filteredServerArray.count;
    }
    else{
        count = (int)self.ServerArray.count;
    }
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        ServerSearchHeaderView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"headerView" forIndexPath:indexPath];
       
        NSString *resultString;
        if(count <= 1)
        {
            resultString = @"Result Found";
        }
        else
        {
            resultString = @"Results Found";
        }
        header.lblHeaderTitle.text = [NSString stringWithFormat:@"%d %@",count,resultString];
        
        headerCell = header;
        
    }
    
    return headerCell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
    
    if(self.ServerArray.count == 0)
    {
        return ;
    }
    
    Servers *newServer = self.ServerArray[indexPath.row];
    
    ServerDetailViewController *serverDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ServerDetailViewController"];
    serverDetailVC.serverID = [NSString stringWithFormat:@"%ld",(long)newServer.serverID];
    
    [self.navigationController pushViewController:serverDetailVC animated:YES];
    
    
}

- (IBAction)loginButtonPressed:(id)sender {
    
    
    LoginView *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self.navigationController pushViewController:login animated:YES];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"Home" forKey:@"Sender"];
    
    
}


#pragma mark collectionview cell action method implementations
- (void)addNewServerPressed:(id)sender {
    NSLog(@"Add Server Pressed");
    
    AddServerViewController *addServer = [self.storyboard instantiateViewControllerWithIdentifier:@"AddServerViewController"];
    
    
        [self.navigationController pushViewController:addServer animated:YES];
//    [self.navigationController  presentViewController:navAddServer animated:YES completion:nil];
    
    
}

- (void)rateButtonPressed:(id)sender {
    UIButton *button = (UIButton*)sender;
    NSLog(@"Button at %ld",(long)button.tag);
    
    //        LoginView *login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    //        [self.navigationController  pushViewController:login animated:YES];
    
    
    ServerDetailViewController *serverDetail = [self.storyboard instantiateViewControllerWithIdentifier:@"ServerDetailViewController"];
    UINavigationController *navServerDetail = [[UINavigationController alloc] initWithRootViewController:serverDetail];
    [self.navigationController  presentViewController:navServerDetail animated:YES completion:nil];
    
}

@end

#pragma mark Empty Cell Flow layout Interface
/*--------------------------Empty Cell Flow Layout------------------------------------*/
@implementation ServerSearchEmptyCellFlowLayout
- (instancetype)init {
    self = [super init];
    if (self)
    {
//        self.minimumLineSpacing = 2.0;
//        self.minimumInteritemSpacing = 1.0;
        self.scrollDirection = UICollectionViewScrollDirectionHorizontal;
//        [self.collectionView setContentOffset:self.collectionView.contentOffset animated:NO];
        
        
    }
    return self;
}

- (CGSize)itemSize {
    
    CGFloat cellWidth = self.collectionView.frame.size.width / 1.2 ;
    CGFloat cellHeight = self.collectionView.frame.size.height / 1.2 ;
    
    return CGSizeMake(cellWidth, cellHeight);
    
}
@end
/*--------------------------------------------------------------*/

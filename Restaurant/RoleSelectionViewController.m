//
//  RoleSelectionViewController.m
//  Restaurant
//
//  Created by HN on 28/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "RoleSelectionViewController.h"
#import <SystemConfiguration/SystemConfiguration.h>

@interface RoleSelectionViewController ()
{
    BOOL isDataDownloaded;
    CLLocation *restaurantLocation;
    
}
@property (nonatomic, strong) RLMResults *restaurantsArray;

@end

@implementation RoleSelectionViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault]; //UIImageNamed:@"transparent.png"
    self.navigationController.navigationBar.shadowImage = [UIImage new];////UIImageNamed:@"transparent.png"
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    UIActivityIndicatorView *activityView = [[UIActivityIndicatorView alloc]
                                             initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    activityView.center=self.view.center;
    [activityView startAnimating];
    [activityView hidesWhenStopped];
    [self.view addSubview:activityView];
    
    
    if ([self isNetworkAvailable]) {
        
        [[LocalData sharedInstance] FetchAllRestaurants:^(BOOL result) {
            if (result)
            {
                NSLog(@"Restaurant Data Downloaded");
                [activityView stopAnimating];
            }
            else{
                [activityView stopAnimating];
            }
        }];
        
        [self initLocationManager];
    }
    else {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Please Check Internet Connectivity"
                                           message:@""
                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:YES completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                
                [alert2 dismissViewControllerAnimated:YES completion:nil];
                return ;
                
            });
            
        });
    }
    
    
    
    
    //    [[LocalData sharedInstance] FetchAllServers:^(BOOL result) {
    //        if (result)
    //        {
    //            NSLog(@"Server Data Downloaded");
    //            [activityView stopAnimating];
    //        }
    //        else{
    //            [activityView stopAnimating];
    //        }
    //    }];
    
    
    
    
    //    locationController = [[AppLocationManager alloc] init];
    //    locationController.delegate= self;
    //    if ([locationController.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    //    {
    //        [locationController.locationManager requestWhenInUseAuthorization];
    //    }
    //
    
    
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:YES];
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"isuserloggedin"] isEqualToString:@"yes"] &&
        [[[NSUserDefaults standardUserDefaults] valueForKey:@"isLoggedIn"] isEqualToString:@"yes"])
    {
        [self goToCustomerHomeView];
        
    }
    
}
- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:YES];
    
    
}
- (void)initLocationManager {
    
    locationController = [[AppLocationManager alloc] init];
    locationController.delegate = self;
    locationController.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    //    [locationController.locationManager startUpdatingLocation];
    if ([locationController.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)])
    {
        [locationController.locationManager requestWhenInUseAuthorization];
    }
    
    if (![CLLocationManager locationServicesEnabled] && [CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)
    {
        UIAlertController * alert2=   [UIAlertController
                                       alertControllerWithTitle:@"Location Services Disabled!"
                                       message:@"Please enable Location Based Services for better results!"
                                       preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *settings = [UIAlertAction actionWithTitle:@"Go To Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString] options:@{} completionHandler:nil];
            
        }];
        [alert2 addAction:settings];
        UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            [alert2 dismissViewControllerAnimated:YES completion:nil];
        }];
        [alert2 addAction:cancel];
        
        [self presentViewController:alert2 animated:YES completion:nil];
        
    }
    else
    {
        //Location Services Enabled, let's start location updates
        [locationController.locationManager startUpdatingLocation];
    }
}

- (void)checkForRestaurantLocation {
    NSLog(@"Latitude: ");
    
    
}

- (IBAction)customerButtonPressed:(id)sender {
    
    [self goToCustomerHomeView];
    
}

- (IBAction)serverButtonPressed:(id)sender {
    [[NSUserDefaults standardUserDefaults] setValue:@"server" forKey:@"UserRole"];
    
}

- (IBAction)restaurantOwnerButtonPressed:(id)sender {
    [[NSUserDefaults standardUserDefaults] setValue:@"restowner" forKey:@"UserRole"];
    
}


- (void)locationUpdate:(CLLocation *)location {
    
    NSLog(@"Location:%@",[location description]);
    
    restaurantLocation = location;
}

- (void)locationError:(NSError *)error {
    
    NSLog(@"Error:%@",[error description]);
    
}

- (void)goToCustomerHomeView {
    self.restaurantsArray  = [Restaurants allObjects];
    NSString *lat;
    NSString *longi;
    lat = @"18.5152592";
    longi = @"73.7114462";
    
    for (Restaurants *object in self.restaurantsArray) {
        
        NSLog(@"Object:%@",object);
        
        if ([object.latitude isEqualToString:lat] && [object.longitude isEqualToString:longi]) {
            
            RestaurantDetailsViewController *restoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RestaurantDetailsViewController"];
            restoVC.restaurantID = [NSString stringWithFormat:@"%ld",(long)object.restroID];
            [self.navigationController presentViewController:restoVC animated:YES completion:nil];
            
            
            
        }
    }
    
    
    [[NSUserDefaults standardUserDefaults] setValue:@"customer" forKey:@"UserRole"];
    
    HomeViewController *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
    
    //    [self.navigationController presentViewController:homeVC animated:YES completion:nil];
    [self.navigationController pushViewController:homeVC animated:YES];

}

- (bool)isNetworkAvailable {
    SCNetworkReachabilityFlags flags;
    SCNetworkReachabilityRef address;
    address = SCNetworkReachabilityCreateWithName(NULL, "www.apple.com" );
    Boolean success = SCNetworkReachabilityGetFlags(address, &flags);
    CFRelease(address);
    
    bool canReach = success
    && !(flags & kSCNetworkReachabilityFlagsConnectionRequired)
    && (flags & kSCNetworkReachabilityFlagsReachable);
    
    return canReach;
}

@end

//
//  AppUser.m
//  Restaurant
//
//  Created by HN on 31/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "AppUser.h"

@implementation AppUser

// Specify default values for properties

//+ (NSDictionary *)defaultPropertyValues
//{
//    return @{};
//}

// Specify properties to ignore (Realm won't persist these)

//+ (NSArray *)ignoredProperties
//{
//    return @[];
//}

@end

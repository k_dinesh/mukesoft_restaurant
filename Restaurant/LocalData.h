//
//  LocalData.h
//  Restaurant
//
//  Created by HN on 17/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "User+CoreDataClass.h"
#import <AFNetworking.h>
#import "Restaurants.h"
#import "RestaurantImages.h"
#import "Servers.h"
#import "AppUser.h"

@interface LocalData : NSObject
@property (strong,nonatomic) NSManagedObjectContext *managedObjectContext;

- (BOOL)loggedInStatus;


+ (instancetype)sharedInstance;


- (void)FetchAllServers:(void (^)(BOOL result))aResult;
- (void)FetchAllRestaurants:(void (^)(BOOL result))aResult;
- (void)rateServer:(NSString *)serverID
            rating:(NSInteger )aRate
        completion:(void (^)(BOOL success))completionBlock;
- (void)commentServer:(NSString *)serverID
              comment:(NSString * )aComment
           completion:(void (^)(BOOL success))completionBlock;
@end

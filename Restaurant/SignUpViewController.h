//
//  SignUpViewController.h
//  Restaurant
//
//  Created by HN on 14/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "AppUser.h"


@interface SignUpViewController : UIViewController <UITextFieldDelegate,NSURLSessionDelegate,UIGestureRecognizerDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UITextField *txtUsername;
@property (weak, nonatomic) IBOutlet UITextField *txtEmailID;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;
@property (weak, nonatomic) IBOutlet UIImageView *userImage;

- (IBAction)loginButtonPressed:(id)sender;

- (IBAction)signupButtonPressed:(id)sender;

@end

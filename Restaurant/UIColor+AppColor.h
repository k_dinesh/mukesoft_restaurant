//
//  UIColor+AppColor.h
//  Restaurant
//
//  Created by HN on 17/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (AppColor)

+ (UIColor *)appMainColor;
+ (UIColor *)textFieldBorderColor;

@end

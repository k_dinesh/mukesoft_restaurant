//
//  RatingSuccessAlertViewController.m
//  Restaurant
//
//  Created by HN on 20/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "RatingSuccessAlertViewController.h"

@interface RatingSuccessAlertViewController ()

@end

@implementation RatingSuccessAlertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    alertBackground.layer.cornerRadius = 10;
    
    self.lblAlertTitle.text = self.alertTitle;
    self.lblAlertMessage.text = self.alertMessage;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (IBAction)closeButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)noButtonPressed:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)yesButtonPressed:(id)sender {
    [[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:@"dismissalert"];
    
    HomeViewController *homeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
    
    //    [self.navigationController presentViewController:homeVC animated:YES completion:nil];

    UINavigationController *navHome = [[UINavigationController alloc] initWithRootViewController:homeVC];
    
    [self presentViewController:navHome animated:YES completion:nil];
    

}
@end

//
//  ServerProfileViewController.h
//  Restaurant
//
//  Created by HN on 17/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RatingSuccessAlertViewController.h"
#import "Servers.h"
#import "LocalData.h"


@class LocalData;




@interface ServerDetailViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *blurrImageview;
@property (weak, nonatomic) IBOutlet UIImageView *serverProfilePic;
@property (weak, nonatomic) IBOutlet UILabel *serverName;
@property (weak, nonatomic) IBOutlet UILabel *totalRatings;
@property (weak, nonatomic) IBOutlet UILabel *restaurantName;
@property (weak, nonatomic) IBOutlet UIImageView *starImage1;
@property (weak, nonatomic) IBOutlet UIImageView *starImage2;
@property (weak, nonatomic) IBOutlet UIImageView *starImage3;
@property (weak, nonatomic) IBOutlet UIImageView *starImage4;
@property (weak, nonatomic) IBOutlet UIImageView *starImage5;

@property (weak, nonatomic) IBOutlet UITextField *txtComment;




@property (weak, nonatomic) IBOutlet UIButton *starButton1;
@property (weak, nonatomic) IBOutlet UIButton *starButton2;
@property (weak, nonatomic) IBOutlet UIButton *starButton3;
@property (weak, nonatomic) IBOutlet UIButton *starButton4;
@property (weak, nonatomic) IBOutlet UIButton *starButton5;

@property (weak,nonatomic) NSString *serverID;


//- (IBAction)ratingButtonPressed:(id)sender;
- (IBAction)starRatingPressed:(UIButton *)sender;
- (IBAction)postCommentButtonPressed:(UIButton *)sender;

- (void)dismissAlert;


@end

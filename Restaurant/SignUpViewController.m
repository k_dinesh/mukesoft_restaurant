//
//  SignUpViewController.m
//  Restaurant
//
//  Created by HN on 14/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "SignUpViewController.h"

@interface SignUpViewController ()
{
        
    NSString *userImageString;
    UIActivityIndicatorView *activityView;
}
@end

@implementation SignUpViewController 

- (void)viewDidLoad{
    [super viewDidLoad];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault]; //UIImageNamed:@"transparent.png"
    self.navigationController.navigationBar.shadowImage = [UIImage new];////UIImageNamed:@"transparent.png"
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar
     setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    _userImage.layer.cornerRadius = _userImage.frame.size.width/2;
    _userImage.layer.masksToBounds = YES;
    _userImage.contentMode = UIViewContentModeScaleAspectFill;

    activityView = [[UIActivityIndicatorView alloc]
                   initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    activityView.center=self.view.center;
    [activityView hidesWhenStopped];
    [self.view addSubview:activityView];

   
    [_txtUsername setDelegate:self];
    [_txtEmailID setDelegate:self];
    [_txtPassword setDelegate:self];
    [_txtConfirmPassword setDelegate:self];
    
    // Image Selection by tapping on imageview
    UITapGestureRecognizer *tapSelectedImage = [[UITapGestureRecognizer alloc]initWithTarget:self
                                                                                      action:@selector(tappedToSelectImage:)];
    tapSelectedImage.delegate = self;
    [tapSelectedImage setNumberOfTapsRequired:1];
    [tapSelectedImage setNumberOfTouchesRequired:1];
    [_userImage addGestureRecognizer:tapSelectedImage];
    [_userImage setUserInteractionEnabled:YES];

    
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}
- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [[self view] endEditing:YES];
}


#pragma mark Image Picker delegate methods

- (IBAction)tappedToSelectImage:(UITapGestureRecognizer *)tapRecognizer {
    
    
    if (tapRecognizer.state == UIGestureRecognizerStateEnded)
    {
        CGFloat frameHeight = _userImage.frame.size.height;
        CGRect imageViewFrame = CGRectInset(_userImage.bounds, 0.0, (CGRectGetHeight(_userImage.frame) - frameHeight) / 2.0 );
        BOOL userTappedOnimageView = (CGRectContainsPoint(imageViewFrame, [tapRecognizer locationInView:_userImage]));
        if (userTappedOnimageView)
        {
            [self selectPhotos];
            
        }
        
    }
    
    
}
- (void)selectPhotos {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = NO;
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Photos" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
        picker.sourceType=UIImagePickerControllerSourceTypePhotoLibrary;
        picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
        
        [self presentViewController:picker animated:YES completion:nil];
        
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:picker.sourceType];
        
        [self presentViewController:picker animated:YES completion:nil];
        
    }]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

// Image Picker Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *originalImage =  info[UIImagePickerControllerOriginalImage];
    _userImage.image = originalImage;
    
    UIImage *tmpImage = [self resizeImage:originalImage];
    
    NSString *imageString = [self encodeToBase64String:tmpImage];
    userImageString = [self encodeToBase64String:tmpImage];
    
//    AppUser *newUser = [[AppUser alloc] init];
//    RLMRealm *realm = [RLMRealm defaultRealm];
//    newUser.profilePic = imageString;
//    
//    [realm transactionWithBlock:^{
//    [realm addObject:newUser];
//    }];
//    
  
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginButtonPressed:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}

- (IBAction)signupButtonPressed:(id)sender {
    
    
    if (([_txtUsername.text isEqualToString:@""]) || ([_txtEmailID.text isEqualToString:@""]) ||
        ([_txtPassword.text isEqualToString:@""]) || ([_txtConfirmPassword.text isEqualToString:@""]))
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@"Please Enter All Information"
                                           message:@"Registration Information can not be Empty"                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:YES completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [alert2 dismissViewControllerAnimated:YES completion:nil];
            });
            
        });
    }
    else if (![self isPasswordMatching:_txtPassword.text andConfirmPassword:_txtConfirmPassword.text]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController * alert2=   [UIAlertController
                                           alertControllerWithTitle:@""
                                           message:@"Passwords Do Not Match"
                                           preferredStyle:UIAlertControllerStyleAlert];
            
            [self presentViewController:alert2 animated:YES completion:nil];
            
            
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                [alert2 dismissViewControllerAnimated:YES completion:nil];
            });
            
            
            
        });
        
    }
    else 
    {
        [activityView startAnimating];
        NSError *error;
        
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",SITE_URL,SIGN_UP_URL]];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                           timeoutInterval:60.0];
        
        [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
        
        [request setHTTPMethod:@"POST"];
        
        NSMutableDictionary *dataDict = [NSMutableDictionary new];
        
        [dataDict setValue:_txtUsername.text forKey:@"username"];
        [dataDict setValue:_txtEmailID.text forKey:@"email"];
        [dataDict setValue:_txtPassword.text forKey:@"password"];
        
        
        [dataDict setValue:[[NSUserDefaults standardUserDefaults] valueForKey:@"UserRole"] forKey:@"role"];
        
//        AppUser *newUser;
//        
//        if(newUser.profilePic.length >0)
//        {
//        [dataDict setValue:newUser.profilePic forKey:@"image"];
//        }
        
//        if (userImageString.length > 0) {
//          [dataDict setValue:userImageString forKey:@"image"];
//        }
        
        
        
        NSData *postData = [NSJSONSerialization dataWithJSONObject:dataDict
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:&error];
        
        NSString *postStr;
        if (! postData) {
            NSLog(@"Got an error: %@", error);
            
        } else {
            NSString *jsonString = [[NSString alloc] initWithData:postData encoding:NSUTF8StringEncoding];
            
            postStr = [NSString stringWithFormat:@"[%@]",jsonString];
            
        }
        
        [request setHTTPBody:postData];
        
        
        NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            
            
            NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            NSLog(@"Response: %@",dataString);
            
            double status = (long)[httpResponse statusCode];
            NSLog(@"response status code: %ld", (long)[httpResponse statusCode]);
            if (data.length > 0) {
                
                
                
                NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
                
                if (error != nil) {
                    NSLog(@"Error parsing JSON.");
                }
                else {
                    
                    NSLog(@"Dict: %@", jsonDict);
                    
                  
                    
                    if (status == 202)
                    {
                        [activityView stopAnimating];
                        if([[[NSUserDefaults standardUserDefaults]valueForKey:@"Sender"] isEqualToString:@"Home"])
                        {
                            
                            
                            dispatch_async(dispatch_get_main_queue(), ^{
                                
                                UIAlertController * alert2=   [UIAlertController
                                                               alertControllerWithTitle:@"Success"
                                                               message:[jsonDict valueForKey:@"result"]
                                                               preferredStyle:UIAlertControllerStyleAlert];
                                
                                [self presentViewController:alert2 animated:YES completion:nil];
                                
                                
                                
                                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                    [alert2 dismissViewControllerAnimated:YES completion:nil];
                                    
                                    NSMutableArray *allViewControllers = [NSMutableArray arrayWithArray:[self.navigationController viewControllers]];
                                    for (UIViewController *aViewController in allViewControllers) {
                                        if ([aViewController isKindOfClass:[HomeViewController class]]) {
                                            [self.navigationController popToViewController:aViewController animated:NO];
                                        }
                                    }
                                });
                                
                            });
                        }
                        
                        NSLog(@"Successfully Registered");
                        
                        
                        
                    }
                    else if (status == 400){
                        
                         [activityView stopAnimating];
                        NSLog(@"Login Failed");
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            UIAlertController * alert2=   [UIAlertController
                                                           alertControllerWithTitle:@"Error"
                                                           message:[jsonDict valueForKey:@"result"]                                                           preferredStyle:UIAlertControllerStyleAlert];
                            
                            [self presentViewController:alert2 animated:YES completion:nil];
                            
                            
                            
                            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                [alert2 dismissViewControllerAnimated:YES completion:nil];
                            });
                        });
                        
                    }
                    
                    
                }
            }
            else {
                 [activityView stopAnimating];
                NSLog(@"Login Failed");
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    UIAlertController * alert2=   [UIAlertController
                                                   alertControllerWithTitle:@"Error"
                                                   message:error.localizedDescription
                                                   preferredStyle:UIAlertControllerStyleAlert];
                    
                    [self presentViewController:alert2 animated:YES completion:nil];
                    
                    
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, alertTime * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [alert2 dismissViewControllerAnimated:YES completion:nil];
                    });
                });
            }
            
            
        }];
        
        [postDataTask resume];
        
        [self clearTextFileds];
    }
    
    
     [activityView stopAnimating];
}

- (void)clearTextFileds{
    _txtUsername.text = @"";
    _txtEmailID.text = @"";
    _txtPassword .text = @"";
    _txtConfirmPassword.text = @"";
    
}

- (BOOL)isPasswordMatching:(NSString*)password andConfirmPassword:(NSString*)confirmPassword {
    if ([password isEqualToString:confirmPassword])
        return true;
    else
        return false;
    
}

-(UIImage *)resizeImage:(UIImage *)image {
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 300.0;
    float maxWidth = 400.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth)
    {
        if(imgRatio < maxRatio)
        {
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio)
        {
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else
        {
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
    
}
@end

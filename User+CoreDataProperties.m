//
//  User+CoreDataProperties.m
//  Restaurant
//
//  Created by HN on 17/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import "User+CoreDataProperties.h"

@implementation User (CoreDataProperties)

+ (NSFetchRequest<User *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"User"];
}


+ (BOOL *)getLoggedInStatus
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"User" inManagedObjectContext:appDelegate.managedObjectContext];
    
    [fetchRequest setEntity:entity];
    
    User *aUser = nil;
    
    
    NSError *error = nil;
    aUser = [[appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error] lastObject];
    
    
    fetchRequest = nil;
    
    if ( !aUser ) {
        NSLog( @"No contacts found!" );
        return nil;
    }
    
    return (BOOL*)aUser.isLoggenIn;
    
}

@dynamic username;
@dynamic password;
@dynamic isLoggenIn;

@end

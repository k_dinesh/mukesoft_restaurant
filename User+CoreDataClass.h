//
//  User+CoreDataClass.h
//  Restaurant
//
//  Created by HN on 17/10/16.
//  Copyright © 2016 Varahi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface User : NSManagedObject



@end

NS_ASSUME_NONNULL_END

#import "User+CoreDataProperties.h"
